// arnAu bellavista 2023
// ElecSim
// Script JS

// Variables
var metode = 'dhondt';

// Calcular els resultats quan es modifica el formulari
document.getElementById('formes').onchange = function(){
	calcul();
};

// Canviar el mètode de càlcul de la repartició d'electes
document.getElementById('metodee').onchange = function(){
	metode = metodee.value;
};

document.getElementById('candAfegir').onclick = function(){
	// Afegir una fila a la taula candidatures
	var candTr = document.createElement("tr");
		candTr.setAttribute('class', 'pista');
		candTr.setAttribute('draggable', 'true');
		var candTd0 = document.createElement("td");
			candTd0.setAttribute('class', 'dragIcon');
			candTd0.innerHTML = "&#10303;";
			candTr.appendChild(candTd0);
		var candTd1 = document.createElement("td");
			candTd1.innerHTML = "<input type='text' class='inputes inputTaula candidatura' name='candiatura' placeholder='Candidatura'>";
			candTr.appendChild(candTd1);
		var candTd2 = document.createElement("td");
			candTd2.setAttribute('class', 'alineaDreta');
			candTd2.innerHTML = "<input type='number' class='inputes inputTaula vots' name='vots' min='0' max='1000000000000' placeholder='0'>";
			candTr.appendChild(candTd2);
		var candTd3 = document.createElement("td");
			candTd3.setAttribute('class', 'percent alineaDreta');
			candTr.appendChild(candTd3);
		var candTd4 = document.createElement("td");
			candTd4.setAttribute('class', 'electes');
			candTr.appendChild(candTd4);
		var candTd5 = document.createElement("td");
			candTd5.setAttribute('class', 'alineaDreta');
			candTd5.innerHTML = "<button type='button' class='candEliminar'>&#10006;</button>";
			candTr.appendChild(candTd5);
		document.getElementById('afegirCand').before(candTr);
	// Activem els buttons de les candidatures
	activarCandidatures();
	// Creem les pistes arrossegables
	pistesArrossegables();
}

// Eliminar candidatura
function activarCandidatures(){
	var candEliminar = document.getElementsByClassName("candEliminar");
	for(let i=0; i<candEliminar.length; i++){
		candEliminar[i].onclick = function(){
			eliminarFila(candEliminar[i].parentElement.parentElement);
		}
	}
}

function eliminarFila(e){
	// Esborrem la fila de la taula
	e.remove();
	// Calcular
	calcul();
	// Reactivem els buttons de les candidatures
	activarCandidatures();
}

// Calcular els resultats
function calcul(){
	// Elements
	var totCand = document.getElementsByClassName("candidatura");
	var totVots = document.getElementsByClassName("vots");
	var totPercent = document.getElementsByClassName("percent");
	var totElectes = document.getElementsByClassName("electes");

	// Agafem les dades
	var dElectes = parseInt(document.getElementById('electes').value);
	var dMinim = parseInt(document.getElementById('minim').value);
		if(!dMinim){ dMinim = 0; }
	var dCens = parseInt(document.getElementById('cens').value);
		if(!dCens){ dCens = 0; }
	var dBlancs = parseInt(document.getElementById('blancs').value);
	var dNuls = parseInt(document.getElementById('nuls').value);
	var dVotsValids = 0, dVotsValidsPercent = 0, dVotsEmesos = 0, dParticipacio = 0, numPercent = 0;
	var rElectes = [], rPercent = [], repartiment = [];
	
	for(let i=0; i<totCand.length; i++){
		if(totVots[i].value){
			dVotsValids += parseInt(totVots[i].value);
		}
	}
	if(dBlancs){
		dVotsValids += dBlancs;
	}
	if(dNuls){
		dVotsEmesos = dVotsValids + dNuls;
	}else{
		dVotsEmesos = dVotsValids;
	}
	
	// Participació / vots emesos
	document.getElementById('votsEmesos').innerHTML = dVotsEmesos;
	if(dCens){
		dParticipacio = dVotsEmesos * (100 / dCens);
		document.getElementById('participacio').innerHTML = dParticipacio.toFixed(2)+"%";
	}else{
		document.getElementById('participacio').innerHTML = "";
	}

	// Vots vàlids
	votsValids.innerHTML = dVotsValids;
	if(dVotsValids && dVotsEmesos){
		dVotsValidsPercent = dVotsValids * (100 / dVotsEmesos);
		document.getElementById('votsValids').nextElementSibling.innerHTML = dVotsValidsPercent.toFixed(2)+"%";
	}
	
	// Calculem el % de vot vàlid per candidatura
	for(let i=0; i<totVots.length; i++){
		if(totVots[i].value){
			var numPercent = parseInt(totVots[i].value) * 100 / dVotsValids;
		}
		totPercent[i].innerHTML = numPercent.toFixed(2)+"%";
		// Guardem el % de la candidatura
		rPercent[i] = numPercent;
		numPercent = 0;
	}

	// % Blancs
	if(dBlancs){
		numPercent = dBlancs * 100 / dVotsValids;
	}
	document.getElementById('percentBlancs').innerHTML = numPercent.toFixed(2)+"%";
	
	// Repartiment d'escons
	if(dElectes){
		// Posem a 0 els electes de cada candidatura
		for(let i=0; i<totVots.length; i++){
			rElectes[i] = 0;
		}
		var partit = '';
		for(let i=0; i<dElectes; i++){
			var max = 0, calcul = 0;
			// var ultimElecte = dElectes - 1;
			for(let j=0; j<totVots.length; j++){
				// Si no supera el mínim no li donem res
				if(rPercent[j] >= dMinim){
					if(metode == 'primer'){
						calcul = parseInt(totVots[j].value);
					}else if(metode == 'adams'){
						calcul = parseInt(totVots[j].value) / rElectes[j];
					}else if(metode == 'danish'){
						calcul = parseInt(totVots[j].value) / (rElectes[j]*3+1);
					}else if(metode == 'huntington'){
						calcul = parseInt(totVots[j].value) / Math.sqrt(rElectes[j]*(rElectes[j]+1));
					}else if(metode == 'imperiali'){
						calcul = parseInt(totVots[j].value) / (rElectes[j]+2);
					}else if(metode == 'webster'){
						calcul = parseInt(totVots[j].value) / (rElectes[j]*2+1);
					}else{
						calcul = parseInt(totVots[j].value) / (rElectes[j]+1);
					}
					if(calcul > max){
						max = calcul;
						partit = j;
					}
				}
				
			}
			// Afegim la candidatura a l'ordre de repartiment
			repartiment[i] = partit;
			// Sumem l'electe a la candidatura
			rElectes[partit]++;
			max = 0;
		}
		
		// Situem els electes a la taula
		for(let i=0; i<rElectes.length; i++){
			totElectes[i].innerHTML = rElectes[i];
		}
	}
}


// Funció per fer les candidatures arrossegables per canviar-les de posició
function pistesArrossegables(){
    var agafat = null;
    var pistesTr = document.getElementsByClassName("pista");
    for(let p=0; p<pistesTr.length; p++){
		// Inici d'arrossegar
        pistesTr[p].ondragstart = function(e){
        //  e.target.style.opacity = "0.25";
            agafat = p;
            e.dataTransfer.setData("Text", e.target.id);
        }
        // Mentre l'arrosseguem
        pistesTr[p].ondrag = function(e){
        }
        // dragenter // Quan està sobre de llocs possibles de deixar anar
        pistesTr[p].ondragenter = function(e){
            if(e.target.classList.contains("dragIcon")){
                if(agafat > p){
                    e.target.parentElement.classList.add('pistaAvall');
                }else if(agafat < p){
                    e.target.parentElement.classList.add('pistaAmunt');
                }
            }
        }
        // dragover // By default, data/elements cannot be dropped in other elements. To allow a drop, we must prevent the default handling of the element
        pistesTr[p].ondragover = function(e){
            e.preventDefault();
        }
        // dragleave // When the draggable p element leaves the droptarget, reset the DIVS's border style
        pistesTr[p].ondragleave = function(e){
            if(agafat != p && e.target.classList.contains("dragIcon")){
                var parEl = e.target.parentElement;
                if(parEl.classList.contains('pistaAvall')){
                    parEl.classList.remove('pistaAvall');
                }
                if(parEl.classList.contains('pistaAmunt')){
                    parEl.classList.remove('pistaAmunt');
                }
            }
        }
        // drop
        pistesTr[p].ondrop = function(e){
			e.preventDefault();
			// Agafem les dades del formulari de la candidatura que arrosseguem
			var candNom = document.getElementsByClassName("candidatura")[agafat].value;
			var candVots = document.getElementsByClassName("vots")[agafat].value;
			
            if(agafat != p && e.target.classList.contains("dragIcon")){
                var parEl = e.target.parentElement;
                    if(parEl.classList.contains('pistaAvall')){
                        parEl.classList.remove('pistaAvall');
                    }
                    if(parEl.classList.contains('pistaAmunt')){
                        parEl.classList.remove('pistaAmunt');
                    }
                // Mirem a quin número de pista es vol posar
                var pos = 0;
                for(let np=0; np<pistesTr.length; np++){
                    if(p==np){ pos=np; }
                }
                // Agafem la pista arrossegada
                var pistaMoure = pistesTr[agafat].innerHTML;
                if(pos < agafat){
                    // Movem les pistes cap avall
                    for(let i=agafat; i>pos; i--){
						var auxNom = pistesTr[i-1].children[1].children[0].value;
						var auxVots = pistesTr[i-1].children[2].children[0].value;
						// Movem la pista
						pistesTr[i].innerHTML = pistesTr[i-1].innerHTML;
						// Omplim els formularis amb la info
						pistesTr[i].children[1].children[0].value = auxNom;
						pistesTr[i].children[2].children[0].value = auxVots;
					}
                }else if(pos > agafat){
                    // Movem les pistes cap amunt
                    for(let i=agafat; i<pos; i++){
						var auxNom = pistesTr[i+1].children[1].children[0].value;
						var auxVots = pistesTr[i+1].children[2].children[0].value;
						// Movem la pista
                        pistesTr[i].innerHTML = pistesTr[i+1].innerHTML;
						// Omplim els formularis amb la info
						pistesTr[i].children[1].children[0].value = auxNom;
						pistesTr[i].children[2].children[0].value = auxVots;
                    }
                }
				// Posem la pista arrossegada a la pista deixada
				e.target.parentNode.innerHTML = pistaMoure;
				// Posem les dades del formulari a la pista final
				pistesTr[pos].children[1].children[0].value = candNom;
				pistesTr[pos].children[2].children[0].value = candVots;
				// Rectivem els buttons de les candidatures
				activarCandidatures();
            }
        }
    }
}